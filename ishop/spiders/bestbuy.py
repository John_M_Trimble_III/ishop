# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule

class BestbuySpider(CrawlSpider):
    name = 'bestbuy'
    allowed_domains = ['bestbuy.com']
    start_urls = ['http://bestbuy.com/']

    rules = (
        Rule(LinkExtractor(allow_domains='bestbuy.com'), callback='parse_item', follow=True),
    )

    def parse_item(self, response):

        if "skuId=" in response.request.url:
            i = {}
            i['name'] = response.xpath('//h1/text()').extract_first()
            
            i['base-price'] = response.xpath('//span[@id="priceblock_ourprice"]/text()').extract_first()
            
            i['used-price'] = response.xpath('//span[@id="used-button-price"]/text()').extract()
            
            i['images'] = response.xpath('//li[contains(@class, "image") and contains(@class, "item")]//img').extract()
            
            i['attributes'] = response.xpath('//div[@id="prodDetails"]//table//tr').extract()
            
            i['brand'] = response.xpath('//a[@id="brand"]/text()').extract_first()
            
            i['category'] = response.xpath('//a[@class="a-link-normal a-color-tertiary"]/text()').extract()
            
            
            return i
