# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
import re
from pdb import set_trace as bp
from selenium import webdriver




# The JavaScript that we want to inject.
# This updates the second `span` with the execution time of the script.
# `arguments[0]` is how Selenium passes in the callback for `execute_async_script()`.
load_jquery = (
	'/** dynamically load jQuery */'\
	'(function(jqueryUrl, callback) {'\
	'   if (typeof jqueryUrl != \'string\') {'\
	'       jqueryUrl = \'https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js\';'\
	'   }'\
	'   if (typeof jQuery == \'undefined\') {'\
	'       var script = document.createElement(\'script\');'\
	'       var head = document.getElementsByTagName(\'head\')[0];'\
	'       var done = false;'\
	'       script.onload = script.onreadystatechange = (function() {'\
	'           if (!done && (!this.readyState || this.readyState == \'loaded\''\
	'           || this.readyState == \'complete\')) {'\
	'               done = true;'\
	'               script.onload = script.onreadystatechange = null;'\
	'               head.removeChild(script);'\
	'               callback();'\
	'           }'\
	'       });'\
	'       script.src = jqueryUrl;'\
	'       head.appendChild(script);'\
	'   }'\
	'   else {'\
	'       callback();'\
	'   }'\
	'   })(arguments[0], arguments[arguments.length - 1]);'\
)

extract_token = (
	'function offset(el) {'\
	'   var rect = el.getBoundingClientRect(),'\
	'   scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,'\
	'   scrollTop = window.pageYOffset || document.documentElement.scrollTop;'\
	'   return { top: rect.top + scrollTop, left: rect.left + scrollLeft }'\
	'}'\
	''\
	'var htmlTags = document.querySelectorAll("body *");'\
	''\
	'var jsonifiedHtmlTags = [];'\
	''\
	'for (var i=0, max = htmlTags.length; i < max; i++) {'\
	''\
	'   /* Do something with the element here */'\
	'   htmlTag = htmlTags[i];'\
	''\
	'   if(jQuery(htmlTag).is(\':visible\'))'\
	'   {'\
	'       var htmlLearningObject = {'\
	'           childElementCount: 0,'\
	'           siblingsCount: 0,'\
	'           elementDepth: 0,'\
	'           elementWidth: 0,'\
	'           elementHeight: 0,'\
	'           offsetX: 0,'\
	'           offsetY: 0,'\
	'           textLength: 0,'\
	'           div: false,'\
	'           h1: false,'\
	'           h2: false,'\
	'           h3: false,'\
	'           h4: false,'\
	'           a: false,'\
	'           table: false,'\
	'           p: false,'\
	'           span: false,'\
	'           el: null'\
	'       };'\
	'       htmlLearningObject.el = htmlTag;'\
	'       htmlLearningObject.childElementCount = jQuery(htmlTag).find(\'*\').length;'\
	'       htmlLearningObject.siblingsCount = jQuery(htmlTag).siblings().length;'\
	'       htmlLearningObject.elementDepth = jQuery(htmlTag).parents().length;'\
	'       htmlLearningObject.elementWidth = htmlTag.offsetWidth;'\
	'       htmlLearningObject.elementHeight = htmlTag.offsetHeight;'\
	''\
	'       /* Get offset using the helper functions */'\
	'       var adjustedBounding = offset(htmlTag);'\
	'       htmlLearningObject.offsetX = adjustedBounding.left;'\
	'       htmlLearningObject.offsetY = adjustedBounding.top;'\
	'       htmlLearningObject.textLength = htmlTag.innerText.length;'\
	''\
	'       var nodeName = htmlTag.nodeName;'\
	''\
	'       if ("DIV" == nodeName)'\
	'       {'\
	'           htmlLearningObject.div = true;'\
	'       }'\
	'       else if("A" == nodeName)'\
	'       {'\
	'           htmlLearningObject.a = true;'\
	'       }'\
	'       else if("H1" == nodeName)'\
	'       {'\
	'           htmlLearningObject.h1 = true;'\
	'       }'\
	'       else if("H2" == nodeName)'\
	'       {'\
	'           htmlLearningObject.h2 = true;'\
	'       }'\
	'       else if("H3" == nodeName)'\
	'       {'\
	'           htmlLearningObject.h3 = true;'\
	'       }'\
	'       else if("H4" == nodeName)'\
	'       {'\
	'           htmlLearningObject.h4 = true;'\
	'       }'\
	'       else if("TABLE" == nodeName)'\
	'       {'\
	'           htmlLearningObject.table = true;'\
	'       }'\
	'       else if("P" == nodeName)'\
	'       {'\
	'           htmlLearningObject.p = true;'\
	'       }'\
	'       else if("SPAN" == nodeName)'\
	'       {'\
	'           htmlLearningObject.span = true;'\
	'       }'\
	'       jsonifiedHtmlTags.push(htmlLearningObject);'\
	'       }'\
	'   }'\
	'return jsonifiedHtmlTags;'\
	)

class TrainingSpider(CrawlSpider):
	name = 'training'
	allowed_domains = ['amazon.com']
	start_urls = ['https://www.amazon.com/']


	#  value contains the link
	@staticmethod
	def remove_garbage_from_urls(value):
		url = value[:value.find('ref')]
		return url[:url.find('?')]


	def remove_html_tags(self, data):
		p = re.compile(r'<.*?>')
		return p.sub('', data)


	def find_token_element_in_tokenified_tags(self, stack_to_search, needle):
		for x in stack_to_search:
			if x['el'] == needle:
				stack_to_search.remove(x)
				return x

	def prepare_training_data(self, tokenified_output): 
		tokenified_output['output_name'] = 0
		tokenified_output['output_brand'] = 0
		tokenified_output['output_description'] = 0
		tokenified_output['output_price'] = 0
		tokenified_output['output_attributes'] = 0
		tokenified_output['output_images'] = 0
		tokenified_output['output_category'] = 0 
		tokenified_output['output_rating'] = 0
		del(tokenified_output['el'])
		return tokenified_output


	def parse_item(self, response):

		if "/dp/" in response.request.url:
			i = []


			driver = webdriver.Chrome()
			options = webdriver.ChromeOptions()
			options.add_argument('headless')

			driver.get(response.url)
			driver.execute_async_script(load_jquery)
			tokenified_tags = driver.execute_script(extract_token)
			

			try: 			
				name = driver.find_element_by_xpath("(//h1 | //h2 | //h3)[contains(@class,'heading') or contains(@id,'title') or contains(@class,'product')][1]")
				tokenified_name = None
				tokenified_name = self.find_token_element_in_tokenified_tags(tokenified_tags, name)
				if tokenified_name:
					tokenified_name = self.prepare_training_data(tokenified_name)
					tokenified_name['output_name'] = 1
					i.append(tokenified_name)				
			except:
				bp()


			try:
				brand = driver.find_element_by_xpath("//*[@id='bylineInfo']")
				tokenified_brand = None
				tokenified_brand = self.find_token_element_in_tokenified_tags(tokenified_tags, brand)
				if tokenified_brand:
					tokenified_brand = self.prepare_training_data(tokenified_brand)
					tokenified_brand['output_brand'] = 1
					i.append(tokenified_brand)						
			except:
				bp()

			return i

	rules = (
		Rule(LinkExtractor(allow_domains='amazon.com', canonicalize=True, unique=True, process_value=remove_garbage_from_urls), callback='parse_item', follow=True),
		# Rule(LinkExtractor(allow_domains='amazon.com', canonicalize=True, unique=True), callback='parse_item', follow=True),

	)

